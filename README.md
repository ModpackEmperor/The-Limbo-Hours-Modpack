## Features

Custom OTG terrain generation, various performance improvements, community patches for increased stability & functionality and warfare related mechanics.

## How to Install

. Install PolyMC and Java 21/23.

> Prism Launcher and MultiMC are compatible too.

. Download the modpack: https://mega.nz/folder/qQswlZQB#LvjcfKv-z_4XlkorYMfdew

. Extract the contents of the modpack to PolyMC's directory.

> Linux: /home/$USER/.local/share/PolyMC

> Windows: %appdata%/PolyMC
  
. Adjust your allocated RAM to 4 - 8 gigabytes.

## How to Setup Multiplayer

. Enable port fowarding, open up a port set on TCP, connect your device's internal IP to the opened port and enable UPnP in your router.

. Type the command `/open <your_opened_port> [cheats=false] [gameMode=Survival]` in Minecraft's console on a singleplayer world.

## Screenshots

<p align="center">
		<img src="https://i.postimg.cc/FHWvFQq4/ima35ge.png" />
